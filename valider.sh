#!/bin/bash

echo 'Sending files ...'
rsync -avzu --delete src/towa/* icanet@info-ssh1.iut.u-bordeaux.fr:~/Remise/towa/info_s1/icanet/depot/towa/

echo 'Updating server ...'
ssh icanet@info-ssh1.iut.u-bordeaux.fr 'cd Remise/towa/info_s1/icanet/; rm sortie.log'
