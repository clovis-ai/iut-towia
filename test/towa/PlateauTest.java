/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Action.Type.ACTIVATION;
import static towa.Action.Type.POSE;
import static towa.Couleur.BLANC;
import static towa.Couleur.NOIR;

/**
 *
 * @author icanet
 */
public class PlateauTest {
    
    public static final Plateau PLATEAU_TEST_2;
    public static final Plateau PLATEAU_TEST_16;
    
    static{
        Tour[][] t = new Tour[][]{
            {new Tour(NOIR, 2), new Tour(NOIR, 1)},
            {new Tour(BLANC, 3), null}
        };
        PLATEAU_TEST_2 = new Plateau(new Plateau(t, NOIR));
        Tour[][] t16 = new Tour[16][16];
        t16[0][0] = new Tour(NOIR, 1);
        t16[5][6] = new Tour(BLANC, 3);
        t16[8][4] = new Tour(NOIR, 2);
        t16[8][6] = new Tour(BLANC, 2);
        t16[12][15] = new Tour(NOIR, 1);
        t16[2][13] = new Tour(NOIR, 4);
        t16[4][12] = new Tour(BLANC, 2);
        PLATEAU_TEST_16 = new Plateau(new Plateau(t16, NOIR));
    }
    
    public PlateauTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testPoser() {
        System.out.println("poser");
        Plateau plateau = new Plateau(PLATEAU_TEST_2, new Action(0, 0, POSE));
        Tour[][] expResult = new Tour[][]{
            {new Tour(NOIR, 2), new Tour(NOIR, 1)},
            {new Tour(BLANC, 3), null}
        };
        Tour[][] r = plateau.copie();
        assertArrayEquals(expResult, plateau.copie());
    }

    /**
     * Test of copie method, of class Plateau.
     */
    @Test
    public void testCopie() {
        System.out.println("copie");
        Plateau instance = new Plateau(PLATEAU_TEST_2);
        Tour[][] expResult = new Tour[][]{
            {new Tour(NOIR, 2), new Tour(NOIR, 1)},
            {new Tour(BLANC, 3), null}
        };
        Tour[][] result = instance.copie();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of tourPresente method, of class Plateau.
     */
    @Test
    public void testTourPresente() {
        System.out.println("tourPresente");
        assertTrue(true);
    }

    /**
     * Test of get method, of class Plateau.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        assertTrue(true);
    }

    /**
     * Test of activationPossible method, of class Plateau.
     */
    @Test
    public void testActivationPossible() {
        System.out.println("activationPossible");
        Plateau p = new Plateau(PLATEAU_TEST_2);
        assertFalse(p.activationPossible(0, 0));
        assertTrue(p.activationPossible(1, 0));
        
        p = new Plateau(PLATEAU_TEST_16);
        assertTrue("Erreur de couleur", p.enfantPossible(new Action(8, 6, ACTIVATION)));
        assertFalse("Erreur de couleur", p.enfantPossible(new Action(8, 4, ACTIVATION)));
        assertFalse("Case vide", p.enfantPossible(new Action(8, 5, ACTIVATION)));
    }

    /**
     * Test of activer method, of class Plateau.
     */
    @Test
    public void testActiver() {
        System.out.print("activer");
        Plateau p = new Plateau(new Plateau(PLATEAU_TEST_2, new Action(1, 0, ACTIVATION)));
        System.out.println(" " + p.couleur());
        Tour[][] expResult = new Tour[][]{
            {null, null},
            {new Tour(BLANC, 3), null}
        };
        assertArrayEquals(expResult, p.copie());
    }

    /**
     * Test of enfantPossible method, of class Plateau.
     */
    @Test
    public void testEnfantPossible() {
        System.out.println("enfantPossible");
        Action a = new Action(0, 0, POSE);
        //a.enfantPossible(POSE);
        
    }

    /**
     * Test of creerEnfant method, of class Plateau.
     */
    @Test
    public void testCreerEnfant() {
        System.out.println("creerEnfant");
        /*Action action = null;
        Plateau instance = null;
        Plateau expResult = null;
        Plateau result = instance.creerEnfant(action);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }

    /**
     * Test of posePossible method, of class Plateau.
     */
    @Test
    public void testPosePossible() {
        System.out.println("posePossible");
        Plateau p = new Plateau(PLATEAU_TEST_16);
        assertTrue("Erreur de couleur", p.enfantPossible(new Action(8, 6, POSE)));
        assertFalse("Erreur de couleur", p.enfantPossible(new Action(8, 4, POSE)));
        
        assertFalse("Erreur de taille", p.enfantPossible(new Action(2, 13, POSE)));
    }

    /**
     * Test of penser method, of class Plateau.
     */
    @Test
    public void testPenser() {
        System.out.println("penser");
        Plateau test = new Plateau(PLATEAU_TEST_2);
        System.out.println("\t JOUEUR:" + test.couleur());
        Action res = test.penser();
        assertTrue(test.enfantPossible(res));
        
        Plateau test2 = new Plateau(PLATEAU_TEST_16);
        System.out.println("\tJOUEUR:" + test2.couleur());
        long t = System.currentTimeMillis();
        Action resultat = test2.penser();
        System.out.println("Fin : " + resultat.toString() + " " + resultat.getPions(NOIR) + " " + resultat.getPions(BLANC));
        assertTrue(test2.enfantPossible(resultat));
        System.out.println("Temps_16 : " + (System.currentTimeMillis() - t) + "ms");
    }

    /**
     * Test of couleur method, of class Plateau.
     */
    @Test
    public void testCouleur() {
        System.out.println("couleur");
        /*Plateau instance = null;
        Couleur expResult = null;
        Couleur result = instance.couleur();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }

    /**
     * Test of hashCode method, of class Plateau.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        /*Plateau instance = null;
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }

    /**
     * Test of equals method, of class Plateau.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        /*Object obj = null;
        Plateau instance = null;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }

}
