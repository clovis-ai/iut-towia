/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Action.Type.ACTIVATION;
import static towa.Couleur.BLANC;

/**
 *
 * @author icanet
 */
public class ActionTest {
    
    public ActionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testToString() {
        Action a = new Action(0, 0, 'P');
        assertEquals("PaA", a.toString());
        
        try{
            Action b = new Action(0, 0, 'D');
            fail();
        }catch(IllegalArgumentException e){
            assertTrue(true);
        }
        
        Action c1 = new Action(0, 0, ACTIVATION);
        Action c2 = new Action(0, 0, 'A');
        assertEquals(c1, c2);
    }

    /**
     * Test of hashCode method, of class Action.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        assertTrue(true);
    }

    /**
     * Test of equals method, of class Action.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        assertTrue(true);
    }

    /**
     * Test of getLigne method, of class Action.
     */
    @Test
    public void testGetLigne() {
        System.out.println("getLigne");
        assertTrue(true);
    }

    /**
     * Test of getColonne method, of class Action.
     */
    @Test
    public void testGetColonne() {
        System.out.println("getColonne");
        assertTrue(true);
    }

    /**
     * Test of getType method, of class Action.
     */
    @Test
    public void testGetAction() {
        System.out.println("getAction");
        assertTrue(true);
    }

    /**
     * Test of reinit method, of class Action.
     */
    @Test
    public void testReinit() {
        System.out.println("reinit");
        assertTrue(true);
    }

    /**
     * Test of ajouter method, of class Action.
     */
    @Test
    public void testAjouter() {
        System.out.println("ajouter");    
        Action a = new Action(0, 0, ACTIVATION);
        a.ajouter(BLANC, 1);
        assertEquals(1, a.getPions(BLANC));
    }

    /**
     * Test of enlever method, of class Action.
     */
    @Test
    public void testEnlever() {
        System.out.println("enlever");
        Action a = new Action(0, 0, ACTIVATION);
        a.ajouter(BLANC, 4);
        a.enlever(BLANC, 1);
        assertEquals(3, a.getPions(BLANC));
    }

    /**
     * Test of getPions method, of class Action.
     */
    @Test
    public void testGetPions() {
        System.out.println("getPions");
        assertTrue(true);
    }

    /**
     * Test of getType method, of class Action.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        assertTrue(true);
    }

    private Object ajouter(Couleur couleur, int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
