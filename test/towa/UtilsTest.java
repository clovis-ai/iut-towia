/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author icanet
 */
public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of numVersCarLigne method, of class Utils.
     */
    @Test
    public void testNumVersCarLigne() {
        System.out.println("numVersCarLigne");
    }

    /**
     * Test of numVersCarColonne method, of class Utils.
     */
    @Test
    public void testNumVersCarColonne() {
        System.out.println("numVersCarColonne");
    }

    /**
     * Test of carLigneVersNum method, of class Utils.
     */
    @Test
    public void testCarLigneVersNum() {
        System.out.println("carLigneVersNum");
    }

    /**
     * Test of carColonneVersNum method, of class Utils.
     */
    @Test
    public void testCarColonneVersNum() {
        System.out.println("carColonneVersNum");
    }

    /**
     * Test of nettoyerTableau method, of class Utils.
     */
    @Test
    public void testNettoyerTableau() {
        System.out.println("nettoyerTableau");
    }

    /**
     * Test of plateauDepuisTexte method, of class Utils.
     */
    @Test
    public void testPlateauDepuisTexte() {
        System.out.println("plateauDepuisTexte");
    }

    /**
     * Test of caseDepuisCodage method, of class Utils.
     */
    @Test
    public void testCaseDepuisCodage() {
        System.out.println("caseDepuisCodage");
    }

    /**
     * Test of afficherActionsPossibles method, of class Utils.
     */
    @Test
    public void testAfficherActionsPossibles() {
        System.out.println("afficherActionsPossibles");
    }

    /**
     * Test of actionsPossiblesContient method, of class Utils.
     */
    @Test
    public void testActionsPossiblesContient() {
        System.out.println("actionsPossiblesContient");
    }

    /**
     * Test of directionVersUtf method, of class Utils.
     */
    @Test
    public void testDirectionVersUtf() {
        System.out.println("directionVersUtf");
    }

    /**
     * Test of caseToString method, of class Utils.
     */
    @Test
    public void testCaseToString() {
        System.out.println("caseToString");
    }

    /**
     * Test of copie method, of class Utils.
     */
    @Test
    public void testCopie() {
        System.out.println("copie");
    }

    /**
     * Test of equalsCase method, of class Utils.
     */
    @Test
    public void testEqualsCase() {
        System.out.println("equalsCase");
    }
    
}
