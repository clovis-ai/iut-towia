/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author icanet
 */
public class TcpGrandOrdonnateurTest {
    
    public TcpGrandOrdonnateurTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of connexion method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testConnexion() throws Exception {
        System.out.println("connexion");
    }

    /**
     * Test of envoiEntier method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testEnvoiEntier() throws Exception {
        System.out.println("envoiEntier");
    }

    /**
     * Test of envoiCaractere method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testEnvoiCaractere() throws Exception {
        System.out.println("envoiCaractere");
    }

    /**
     * Test of receptionEntier method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testReceptionEntier() throws Exception {
        System.out.println("receptionEntier");
    }

    /**
     * Test of receptionCaractere method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testReceptionCaractere() throws Exception {
        System.out.println("receptionCaractere");
    }

    /**
     * Test of deconnexion method, of class TcpGrandOrdonnateur.
     */
    @Test
    public void testDeconnexion() throws Exception {
        System.out.println("deconnexion");
    }
    
}
