/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Couleur.NOIR;

/**
 *
 * @author icanet
 */
public class TourTest {
    
    public TourTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of peutPoser method, of class Tour.
     */
    @Test
    public void testPeutPoser() {
        System.out.println("peutPoser");
        int nbPions = 2;
        Tour instance = new Tour(NOIR, nbPions);
        assertFalse(instance.peutPoser(3));
        assertTrue(instance.peutPoser(2));
    }

    /**
     * Test of poser method, of class Tour.
     */
    @Test
    public void testPoser() {
        System.out.println("poser");
        Tour tour = new Tour(NOIR, 2);
        tour = tour.poser(1);
        assertEquals(new Tour(NOIR, 3), tour);
    }

    /**
     * Test of getCouleur method, of class Tour.
     */
    @Test
    public void testGetCouleur() {
        System.out.println("getCouleur");
        assertTrue(true);
    }

    /**
     * Test of getHauteur method, of class Tour.
     */
    @Test
    public void testGetHauteur() {
        System.out.println("getHauteur");
        assertTrue(true);
    }

    /**
     * Test of toString method, of class Tour.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        assertTrue(true);
    }

    /**
     * Test of hashCode method, of class Tour.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
       /* Tour instance = null;
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }

    /**
     * Test of equals method, of class Tour.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        /*Object obj = null;
        Tour instance = null;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");*/
    }
    
}
