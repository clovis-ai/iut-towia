/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static towa.Action.Type.POSE;
import static towa.Couleur.BLANC;

/**
 *
 * @author icanet
 */
public class TowiaTest {
    
    public TowiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGame(){
        /*System.out.println("Game");
        Towia t = new Towia(BLANC);
        t.actionAdversaire(new Action("PmN"));
        t.actionAdversaire(new Action("PaA"));
        t.actionAdversaire(new Action("PoK"));
        t.actionAdversaire(new Action("PaA"));
        t.actionAdversaire(new Action("PaK"));
        t.actionAdversaire(new Action("PaA"));
        t.actionAdversaire(new Action("PaB"));
        t.actionAdversaire(new Action("PaA"));
        t.actionAdversaire(new Action("PkO"));
        t.actionAdversaire(new Action("PaC"));
        t.actionAdversaire(new Action("PnL"));
        t.actionAdversaire(new Action("PaC"));
        t.actionAdversaire(new Action("PbD"));
        t.actionAdversaire(new Action("PaC"));
        t.actionAdversaire(new Action("PfE"));
        Tour[][] p = t.plateau().copie();
        for(Tour[] l : p){
            for(Tour c : l)
                System.out.print(c == null ? "   " : c.toString() + " ");
            System.out.println();
        }
        System.out.println(t.jouer());*/
    }

    /**
     * Test of actionAdversaire method, of class Towia.
     */
    @Test
    public void testActionAdversaire() {
        System.out.println("actionAdversaire");
        Plateau p = new Plateau(BLANC);
        assert p.enfantPossible(new Action(0, 4, POSE));
        p = new Plateau(p, new Action(0, 4, POSE));
        //p = new Plateau(p, new Action(0, 4, POSE));
        Action r = p.penser();
        assert p.enfantPossible(r);
        System.out.println("\t" + r.toString());
    }

    /**
     * Test of jouer method, of class Towia.
     */
    @Test
    public void testJouer() {
        System.out.println("jouer");
    }
    
}
