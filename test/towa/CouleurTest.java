/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Couleur.BLANC;
import static towa.Couleur.NOIR;

/**
 *
 * @author icanet
 */
public class CouleurTest {
    
    public CouleurTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of values method, of class Couleur.
     */
    @Test
    public void testValues() {
        System.out.println("values");
        assertTrue(true);
    }

    /**
     * Test of valueOf method, of class Couleur.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        assertTrue(true);
    }

    /**
     * Test of estNoir method, of class Couleur.
     */
    @Test
    public void testEstNoir() {
        System.out.println("estNoir");
        assertTrue(true);
    }

    /**
     * Test of toChar method, of class Couleur.
     */
    @Test
    public void testToChar() {
        System.out.println("toChar");
        assertTrue(true);
    }

    /**
     * Test of inverse method, of class Couleur.
     */
    @Test
    public void testInverse() {
        System.out.println("inverse");
        Couleur c = BLANC;
        assertEquals(NOIR, c.inverse());
    }
    
}
