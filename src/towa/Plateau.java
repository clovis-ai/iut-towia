/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import java.util.Arrays;
import towa.Action.Type;
import static towa.Utils.ADJACENT;

/**
 * Un plateau de jeu. Un plateau est immuable.
 * <p>Lorsqu'un nouveau plateau est créé :
 * <ul>
 *  <li>Plateau racine</li>
 *  <li>Plateau enfant
 *  <ul>
 *   <li>Ajouter comme enfant au parent</li>
 *   <li>Générer si besoin</li>
 *  </ul>
 *  </li>
 * </ul>
 * @author icanet
 */
public class Plateau {
    
    private final Tour[][] plateau;
    private Couleur joueur;
    private Action action = null;
    
    private boolean peutModifier = true;
    
    /** Taille du plateau. */
    public final int TAILLE;
    
    /**
     * Nombre de tours d'avance à prévoir.
     */
    public final int NOMBRE_RECURSIONS = 6;
    
    private final int[] pions = new int[]{0, 0};
    
    /**
     * Crée un tableau de 16*16
     * @param tour qui va jouer ?
     */
    public Plateau(Couleur tour){
        this(16, tour);
    }
    
    /**
     * Crée un tableau.
     * @param taille taille du tableau
     * @param tour qui va jouer ?
     */
    public Plateau(int taille, Couleur tour){
        TAILLE = taille;
        plateau = new Tour[TAILLE][TAILLE];
        joueur = tour;
        peutModifier = false;
    }
    
    /**
     * Crée un nouveau plateau qui est le précédent avec UNE modification appliquée.
     * <p>On suppose que ce constructeur n'est appelé que si l'action est possible,
     * ce n'est donc pas vérifié.
     * @param plateau l'état de la partie au tour précédent
     * @param action l'action à effectuer
     */
    public Plateau(Plateau plateau, Action action){
        this(plateau.copie(), plateau.joueur.inverse());
        this.action = action;
        switch(action.getType()){
            case POSE: pose(action.getLigne(), action.getColonne()); break;
            case ACTIVATION: activation(action.getLigne(), action.getColonne()); break;
            default: throw new IllegalArgumentException("Le type d'action est invalide : " + action.getType());
        }
        compterPions();
    }
    
    /**
     * Compte le nombre de pions sur le plateau.
     */
    private void compterPions(){
        action.reinit();
        for(int x = 0; x < TAILLE; x++)
            for(int y = 0; y < TAILLE; y++)
                if(get(x, y) != null)
                    action.ajouter(get(x, y).getCouleur(), get(x, y).getHauteur());
    }
    
    /**
     * Pose un ou deux pion(s) sur le plateau.
     * @param ligne ligne de la pose
     * @param colonne colonne de la pose
     */
    private void pose(final int ligne, final int colonne){
        Tour principale = get(ligne, colonne);
        if(principale == null){
            for(int[] dirs : ADJACENT){
                try{
                    Tour adj = get(ligne + dirs[0], colonne + dirs[1]);
                    if(adj != null && adj.getCouleur() != joueur){
                        // Le joueur peut forcément poser, puisque la case est vide
                        plateau[ligne][colonne] = new Tour(joueur, 2);
                        return;
                    }
                }catch(ArrayIndexOutOfBoundsException e){}
            }
            plateau[ligne][colonne] = new Tour(joueur, 1);
        }else{
            if(principale.getCouleur() != joueur)
                return;
            else if(principale.peutPoser(1))
                plateau[ligne][colonne] = principale.poser(1);
        }
    }
    
    /**
     * Active une certaine tour.
     * @param ligne ligne de la tour
     * @param colonne colonne de la tour
     */
    private void activation(final int ligne, final int colonne){
        Tour principale = get(ligne, colonne);
        for(int[] dirs : ADJACENT){
            int maxDist = dirs[0] == 0 || dirs[1] == 0 ? TAILLE : 1;
            int lig = ligne,
                col = colonne;
            try{
                for(int i = 0; i < maxDist; i++){
                    lig += dirs[0];
                    col += dirs[1];
                    Tour adj = get(lig, col);
                    if(adj != null 
                            && adj.getCouleur() != joueur
                            && adj.getHauteur() < principale.getHauteur()){
                        plateau[lig][col] = null;
                    }
                }
            }catch(ArrayIndexOutOfBoundsException e){}
        }
    }
    
    /**
     * Copie le plateau.
     * @param plateau le plateau à copier
     * @see #copie() Copier uniquement le contenu du plateau
     */
    public Plateau(Plateau plateau){
        this(plateau.copie(), plateau.couleur());
    }
    
    /**
     * Crée un objet plateau à partir d'une référence.
     * <p><b>Attention !</b> Ce constructeur ne fait pas de copie, le plateau est
     * une référence !
     * @param plateau la plateau à utiliser
     * @param couleur qui va jouer ?
     */
    public Plateau(Tour[][] plateau, Couleur couleur){
        if(plateau == null)
            throw new IllegalArgumentException("Le plateau doit exister : null");
        TAILLE = plateau.length;
        this.plateau = plateau;
        joueur = couleur;
        peutModifier = false;
    }
    
    /**
     * Est-il possible de jouer une certaine action sur ce plateau ?
     * <p>Cette méthode ne crée par de nouveau tableau.
     * @param action l'action à analyser
     * @return <code>true</code> si on peut générer cette action.
     */
    public boolean enfantPossible(Action action){
        switch(action.getType()){
            case POSE: return posePossible(action.getLigne(), action.getColonne());
            case ACTIVATION: return activationPossible(action.getLigne(), action.getColonne());
            default:
                throw new IllegalArgumentException("Le type de l'action est invalide : " + action.getType());
        }
    }
    
    /**
     * Crée un enfant de ce plateau.
     * @param action l'action permettant de l'atteindre
     * @return L'enfant de ce plateau
     */
    public Plateau creerEnfant(Action action){
        return new Plateau(this, action);
    }
    
    /**
     * Est-il possible de poser un jeton à cet endroit ?
     * @param ligne ligne où on veut poser
     * @param colonne colonne où on veut poser
     * @return <code>true</code> si on peut y poser un jeton.
     */
    public boolean posePossible(int ligne, int colonne){
        Tour principale = get(ligne, colonne);
        return principale == null || principale.getCouleur() == joueur.inverse() && principale.peutPoser(1);
    }
    
    /**
     * Est-il possible d'activer cette tour ?
     * @param ligne ligne où on veut activer
     * @param colonne colonne où on veut activer
     * @return <code>true</code> si on peut y activer un jeton.
     */
    public boolean activationPossible(int ligne, int colonne){
        Tour principale = get(ligne, colonne);
        return principale != null && principale.getCouleur() == joueur.inverse();
    }
    
    /**
     * Permet de choisir quels seront les prochaines actions.
     */
    public Action penser(){
        //s[0] = "";
        //for(int i = 1; i < 10; i++)     s[i] = s[i-1] + " ";
        return penser(NOMBRE_RECURSIONS);
    }
    
    private static final String[] s = new String[10];
    
    /**
     * Permet de choisir quels seront les prochaines actions.
     * @param tours nombre de tours de récursivité
     */
    private Action penser(int tours){
        Action meilleure = null;
        //System.out.println(s[NOMBRE_RECURSIONS-tours] + ">Début");
        for(Type t : Type.values())
            for(int x = 0; x < TAILLE; x++)
                for(int y = 0; y < TAILLE; y++){
                    Action a = new Action(x, y, t);
                    if(enfantPossible(a)){
                        Plateau p = creerEnfant(a);
                        //System.out.println(s[NOMBRE_RECURSIONS-tours+1] + ":" + a + " " + a.getPions(joueur) + " " + a.getPions(joueur.inverse()));
                        if(tours > 0 && t == Type.ACTIVATION){
                            Action a2 = p.penser(tours-1);
                            if(meilleure == null
                                    || a2.estMeilleureQue(meilleure, joueur.inverse())
                                    && a2.estMeilleureQue(a, joueur.inverse()))
                                meilleure = a;
                        }else if(meilleure == null || a.estMeilleureQue(meilleure, joueur.inverse()))
                            meilleure = a;
                    }
                }
        assert meilleure != null : "Il n'y a aucune action valide !";
        //System.out.println(s[NOMBRE_RECURSIONS-tours] + "<Fin:" + meilleure.toString());
        return meilleure;
    }
    
    /**
     * Permet de savoir si une tour est présente dans une certaine case.
     * @param ligne ligne de la case qui nous intéresse
     * @param colonne colonne de la case qui nous intéresse
     * @return <code>true</code> si une tour est présente.
     * @see #get(int, int) Récupère une certaine case
     */
    public boolean tourPresente(int ligne, int colonne){
        return get(ligne, colonne) != null;
    }
    
    /**
     * Récupère une certaine case.
     * @param ligne ligne de la case qui nous intéresse
     * @param colonne colonne de la case qui nous intéresse
     * @return Renvoie la case, ou <code>null</code> si aucune tour n'est présente.
     * @see #tourPresente(int, int) Savoir si une tour est présente
     */
    public Tour get(int ligne, int colonne){
        return plateau[ligne][colonne];
    }
    
    /**
     * De qui est-ce le tour de jouer ?
     * @return La couleur du joueur qui a joué pour arriver à ce plateau.
     */
    public Couleur couleur(){
        return joueur;
    }
    
    /**
     * Copie le plateau de jeu.
     * @return Renvoie une copie du plateau de jeu.
     * @see #Plateau(towa.Plateau) Copier l'objet plateau lui-même
     */
    public Tour[][] copie(){
        Tour[][] cp = new Tour[TAILLE][TAILLE];
        for(int x = 0; x < TAILLE; x++)
            for(int y = 0; y < TAILLE; y++)
                if(get(x, y) != null)
                    cp[x][y] = new Tour(get(x, y));
        return cp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Arrays.deepHashCode(this.plateau);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)                        return true;
        if (obj == null)                        return false;
        if (getClass() != obj.getClass())       return false;
        
        final Plateau other = (Plateau) obj;
        if (this.TAILLE != other.TAILLE)        return false;
        if (!Arrays.deepEquals(this.plateau, other.plateau))
                                                return false;
        return true;
    }
}
