/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import static towa.Couleur.BLANC;
import static towa.Couleur.NOIR;

/**
 *
 * @author icanet
 */
public class Towia {
    
    private Plateau plateau;
    private final Couleur joueur;
    
    /**
     * Crée une IA.
     * @param couleur la couleur du premier joueur 
     */
    public Towia(Couleur couleur){
        joueur = couleur;
        plateau = new Plateau(NOIR);
        System.out.println("Je suis le joueur " + couleur);
    }
    
    /**
     * Crée une IA.
     * @param ordre la couleur du premier joueur : 1 noir, 2 blanc 
     */
    public Towia(int ordre){
        this(ordre == 1 ? NOIR : BLANC);
    }
    
    public void actionAdversaire(Action ennemie){
        plateau = plateau.creerEnfant(ennemie);
    }
    
    public Action jouer(){
        Action ret = plateau.penser();
        assert plateau.enfantPossible(ret);
        plateau = plateau.creerEnfant(ret);
        return ret;
    }
    
    protected Plateau plateau(){
        return plateau;
    }
    
}
