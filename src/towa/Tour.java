/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import java.util.Objects;

/**
 * Représente une tour. Cette classe est immuable.
 * @author icanet
 */
public class Tour {
    
    private final Couleur couleur;
    
    private final int hauteur;
    
    public static final int HAUTEUR_MAX = 4;
    
    /**
     * Crée une nouvelle tour.
     * @param couleur couleur de la tour
     * @param hauteur hauteur de la tour
     */
    public Tour(Couleur couleur, int hauteur){
        if(hauteur > HAUTEUR_MAX)
            throw new IllegalArgumentException("La hauteur ne peut pas dépasser " + HAUTEUR_MAX + ", vous avez mis : " + hauteur);
        
        this.couleur = couleur;
        this.hauteur = hauteur;
    }
    
    /**
     * Crée une copie d'une tour.
     * @param tour la tour à copier
     */
    public Tour(Tour tour){
        couleur = tour.getCouleur();
        hauteur = tour.getHauteur();
    }
    
    /**
     * Peut-on poser de nouveaux pions dans cette case ?
     * @param nbPions le nombre de pions que l'on souhaite poser en plus
     * @return <code>true</code> si on peut <b>tous</b> les poser, <code>false</code> sinon.
     */
    public boolean peutPoser(int nbPions){
        return hauteur + nbPions <= HAUTEUR_MAX;
    }
    
    /**
     * Poser des pions sur cette case.
     * <p>Attention, cette classe est immuable : cet objet N'EST PAS modifié par
     * cette méthode.
     * @param nbPions le nombre de pions 
     * @return Le résultat de l'ajoût du nombre de pions demandés.
     */
    public Tour poser(int nbPions){
        if(!peutPoser(nbPions))
            throw new IllegalStateException("Il n'est pas autorisé de poser un jeton dans cette case : " + toString() + ".peutPoser(" + nbPions + ") == false");
        return new Tour(couleur, hauteur + nbPions);
    }
    
    /**
     * Récupère la couleur de cette tour.
     * @return la couleur de cette tour
     */
    public Couleur getCouleur(){
        return couleur;
    }
    
    /**
     * Récupère la hauteur de cette tour.
     * @return la hauteur de cette tour
     */
    public int getHauteur(){
        return hauteur;
    }
    
    @Override
    public String toString(){
        return couleur.toChar() + "" + hauteur;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.couleur);
        hash = 23 * hash + this.hauteur;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tour other = (Tour) obj;
        if (this.hauteur != other.hauteur) {
            return false;
        }
        if (this.couleur != other.couleur) {
            return false;
        }
        return true;
    }
    
    
    
}
