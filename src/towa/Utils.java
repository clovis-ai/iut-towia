package towa;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Quelques fonctions utiles au projet. Vous devez comprendre ce que font ces
 * méthodes (voir leur documentation), mais pas comment elles le font (leur
 * code).
 */
public class Utils {

    /**
     * Indice de la première ligne (et colonne).
     */
    final static int NUM_LIGNE_MIN = 0;

    /**
     * Indice de la dernière ligne (et colonne).
     */
    final static int NUM_LIGNE_MAX = 15;

    /**
     * Caractère de la première ligne.
     */
    final static char CAR_PREMIERE_LIGNE = 'a';

    /**
     * Caractère de la première colonne.
     */
    final static char CAR_PREMIERE_COLONNE = 'A';

    /**
     * Caractère pour indiquer une tour noire.
     */
    final static char CAR_NOIR = 'N';

    /**
     * Caractère pour indiquer une tour blanche.
     */
    final static char CAR_BLANC = 'B';

    /**
     * Caractère pour indiquer une case vide.
     */
    final static char CAR_VIDE = ' ';

    /**
     * Caractère pour indiquer une case en eau.
     */
    final static char CAR_EAU = 'E';
    
    /**
     * Convertit un numéro de ligne (par exemple 2) en nom de ligne (ici 'c').
     *
     * @param numLigne le numéro de ligne à convertir
     * @return le caractère pour cette ligne
     */
    public static char numVersCarLigne(final int numLigne) {
        if ((numLigne < NUM_LIGNE_MIN) || (numLigne > NUM_LIGNE_MAX)) {
            throw new IllegalArgumentException(
                    "Appel incorrect à numVersCarLigne, avec numLigne = "
                    + numLigne
                    + ". Les valeurs autorisées sont les entiers entre "
                    + NUM_LIGNE_MIN + " et " + NUM_LIGNE_MAX);
        }
        return (char) (CAR_PREMIERE_LIGNE + numLigne);
    }

    /**
     * Convertit un numéro de colonne (par exemple 2) en nom de colonne (ici
     * 'C').
     *
     * @param numColonne le numéro de colonne à convertir
     * @return le caractère pour cette ligne
     */
    public static char numVersCarColonne(final int numColonne) {
        if ((numColonne < NUM_LIGNE_MIN) || (numColonne > NUM_LIGNE_MAX)) {
            throw new IllegalArgumentException(
                    "Appel incorrect à numVersCarColonne, avec numLigne = "
                    + numColonne
                    + ". Les valeurs autorisées sont les entiers entre "
                    + NUM_LIGNE_MIN + " et " + NUM_LIGNE_MAX);
        }
        return (char) (CAR_PREMIERE_COLONNE + numColonne);
    }

    /**
     * Convertit un nom de ligne (par exemple 'c') en numéro de ligne (ici 2).
     *
     * @param nomLigne le nom de ligne à convertir
     * @return le numéro de cette ligne
     */
    public static int carLigneVersNum(final char nomLigne) {
        final char carMin = CAR_PREMIERE_LIGNE + NUM_LIGNE_MIN;
        final char carMax = CAR_PREMIERE_LIGNE + NUM_LIGNE_MAX;
        if ((nomLigne < carMin) || (nomLigne > carMax)) {
            throw new IllegalArgumentException(
                    "Appel incorrect à carVersNum, avec car = " + nomLigne
                    + ". Les valeurs autorisées sont les caractères entre "
                    + carMin + " et " + carMax + ".");
        }
        return nomLigne - CAR_PREMIERE_LIGNE;
    }

    /**
     * Convertit un nom de colonnes (par exemple 'C') en numéro de colonne (ici
     * 2).
     *
     * @param nomColonne le nom de colonne à convertir
     * @return le numéro de cette colonne
     */
    public static int carColonneVersNum(final char nomColonne) {
        final char carMin = CAR_PREMIERE_COLONNE + NUM_LIGNE_MIN;
        final char carMax = CAR_PREMIERE_COLONNE + NUM_LIGNE_MAX;
        if ((nomColonne < carMin) || (nomColonne > carMax)) {
            throw new IllegalArgumentException(
                    "Appel incorrect à carVersNum, avec car = " + nomColonne
                    + ". Les valeurs autorisées sont les caractères entre "
                    + carMin + " et " + carMax + ".");
        }
        return nomColonne - CAR_PREMIERE_COLONNE;
    }

    /**
     * Fonction qui renvoie une copie du tableau sans les cases non utilisées,
     * c'est-à-dire contenant null ou la chaîne vide. Par exemple {"Coucou", "",
     * null, "Hello", null} renvoie {"Coucou", "Hello"}.
     *
     * @param actions le tableau à nettoyer
     * @return le tableau nettoyé
     */
    public static String[] nettoyerTableau(final String[] actions) {
        return Arrays.stream(actions)
                .filter(a -> ((a != null) && (!"".equals(a))))
                .collect(Collectors.toList())
                .toArray(new String[0]);
    }

    /**
     * Construit un plateau à partir de sa représentation sour forme texte,
     * comme renvoyé par formatTexte(), avec coordonnées et séparateurs.
     *
     * @param texteOriginal le texte du plateau
     * @return le plateau
     */
    public static Case[][] plateauDepuisTexte(final String texteOriginal) {
        final Case[][] plateau = new Case[16][16];
        final String[] lignes = texteOriginal.split("\n");
        for (int lig = 0; lig < 16; lig++) {
            final String ligne = lignes[2 * (lig + 1)];
            for (int col = 0; col < 16; col++) {
                String codage = ligne.substring(2 + 4 * col, 2 + 4 * col + 3);
                plateau[lig][col] = caseDepuisCodage(codage);
            }
        }
        return plateau;
    }

    /**
     * Construit une case depuis son codage (affichage dans PlateauTerminal).
     *
     * @param codage codage de la case
     * @return case correspondante
     */
    public static Case caseDepuisCodage(final String codage) {
        final Case laCase = new Case(false, false, 0, 0, 0);
        // 1er caractère : couleur tour / nature
        final char codageNature = codage.charAt(0);
        switch (codageNature) {
            case CAR_NOIR:
                laCase.tourPresente = true;
                laCase.estNoire = true;
                break;
            case CAR_BLANC:
                laCase.tourPresente = true;
                laCase.estNoire = false;
                break;
            case CAR_VIDE:
                laCase.tourPresente = false;
                break;
            case CAR_EAU:
                laCase.nature = 1;
                laCase.tourPresente = false;
            default:
                System.out.println("Nature inconnue");
                break;
        }
        // 2ème caractère : hauteur
        if (laCase.tourPresente) {
            final char carHauteur = codage.charAt(1);
            laCase.hauteur = new Integer("" + carHauteur);
        }
        // 3ème caractère : altitude
        final char carAltitude = codage.charAt(2);
        if (carAltitude != ' ') {
            laCase.altitude = new Integer("" + carAltitude);
        }
        return laCase;
    }

    /**
     * Afficher les action possibles déjà calculées.
     *
     * @param actionsPossibles les actions possibles calculées
     */
    static void afficherActionsPossibles(String[] actionsPossibles) {
        System.out.println(Arrays.deepToString(actionsPossibles));
    }

    /**
     * Indique si une action est présente parmi les actions possibles calculées.
     * 
     * @param actionsPossibles actions possibles calculées
     * @param action l'action à tester
     * @return vrai ssi l'action est présente parmi les actions possibles
     */
    static boolean actionsPossiblesContient(String[] actionsPossibles,
            String action) {
        return Arrays.asList(actionsPossibles).contains(action);
    }
    
    /**
     * Obtient une direction sous forme de flèche (UTF-8).
     * @param x direction en lignes {-1,0,1}
     * @param y direction en colonnes {-1,0,1}
     * @return La direction
     */
    static String directionVersUtf(int x, int y){
        switch(x){
            case -1:
                switch(y){
                    case -1: return "↖";
                    case 0:  return "↑";
                    case 1:  return "↗";
                    default: return "?";
                }
            case 0:
                switch(y){
                    case -1: return "←";
                    case 0:  return "•";
                    case 1:  return "→";
                    default: return "?";
                }
            case 1:
                switch(y){
                    case -1: return "↙";
                    case 0:  return "↓";
                    case 1:  return "↘";
                    default: return "?";
                }
            default: return "?";
        }
    }
    
    /**
     * Génère une chaîne de caractères représentant une case.
     * @param c la case en question
     * @return Une chaîne de caractères correspondant à la méthode d'origine.
     */
    public static String caseToString(Case c){
        if(c.tourPresente)
            return (c.estNoire ? "N" : "B") + c.hauteur + c.altitude + " (niv. " + (c.altitude + c.hauteur) + ")";
        else
            return "[null]";
    }
    
    /**
     * Cette enum contient une direction.
     */
    public enum Direction{
        LIGNE,
        COLONNE;
    }
    
    /**
     * Contient les différents points cardinaux.
     */
    public static final int[][] CARDINAUX;
    
    /**
     * Contient les différentes coordonnées adjacentes.
     */
    public static final int[][] ADJACENT;
    
    static{
        CARDINAUX = new int[4][2];
        CARDINAUX[0] = new int[]{ 0,  1};
        CARDINAUX[1] = new int[]{ 1,  0};
        CARDINAUX[2] = new int[]{ 0, -1};
        CARDINAUX[3] = new int[]{-1,  0};
        ADJACENT = new int[8][2];
        int i = 0;
        System.out.println("Création des cardinalités ...");
        for(int x = -1; x <= 1; x++)
            for(int y = -1; y <= 1; y++)
                if(x != 0 || y != 0){
                    System.out.println(x + " " + y);
                    ADJACENT[i] = new int[]{x, y};
                    i++;
                }
    }
    
    /**
     * Copie le tableau donné en paramètre. Les références vers le contenu sont
     * gardées.
     * @param plateau plateau à copier
     * @return Un copie du plateau.
     */
    public static final Case[][] copie(Case[][] plateau){
        Case[][] ret = new Case[plateau.length][plateau.length];
        for(int i = 0; i < plateau.length; i++)
            for(int j = 0; j < plateau.length; j++)
                ret[i][j] = plateau[i][j];
        return ret;
    }
    
    /**
     * Cette fonction permet de savoir si deux cases sont identiques.
     * <p>Deux cases sont identiques si tous leurs attributs sont identiques.
     * <p>Par convention, <code>null</code> n'est égal à aucune case, même elle-même.
     * <p>Cette fonction est pensée comme l'équivalente de <code>a.equals(b)</code>.
     * @param a la première case à tester
     * @param b la deuxième case à tester
     * @return <code>true</code> si les deux cases sont égales.
     */
    public static boolean equalsCase(Case a, Case b){
        if(a == b)                      return true;
        if(a == null || b == null)      return false;
        return a.tourPresente == b.tourPresente &&
               a.estNoire == b.estNoire &&
               a.altitude == b.altitude &&
               a.hauteur == b.hauteur &&
               a.nature == b.nature;
    }
}
