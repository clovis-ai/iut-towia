/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

/**
 * Symbolise une couleur. On peut l'utiliser de deux
 * manières : (avec une variable <code>Couleur c</code>)
 * <ul>
 *  <li><code>if(c.estNoir()) ...</code></li>
 *  <li><code>if(c == NOIR) ...</code></li>
 * </ul>
 * @author icanet
 */
public enum Couleur {
    
    /**
     * La couleur est noire.
     * @see Couleur Méthode alternative de comparaison
     */
    NOIR(true),
    
    /**
     * La couleur est blanche.
     * @see Couleur Méthode alternative de comparaison
     */
    BLANC(false);
    
    private final boolean estNoir;
    
    private Couleur(boolean estNoir){
        this.estNoir = estNoir;
    }
    
    /**
     * La couleur est-il noir ?
     * @return <code>true</code> si la couleur est noire
     * @see Couleur Méthode alternative de comparaison
     */
    public boolean estNoir(){
        return estNoir;
    }
    
    /**
     * Renvoie N ou B selon que cette couleur est noire ou blanche.
     * @return N ou B
     */
    public char toChar(){
        return estNoir ? 'N' : 'B';
    }
    
    /**
     * Renvoie l'inverse de cet objet.
     * @return BLANC devient NOIR ; NOIR devient BLANC.
     */
    public Couleur inverse(){
        return estNoir ? BLANC : NOIR;
    }
    
}
