/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import java.util.Arrays;
import java.util.Objects;
import static towa.Action.Type.POSE;
import static towa.Couleur.BLANC;
import static towa.Couleur.NOIR;

/**
 * Une action. Cet objet est immuable.
 * @author schourouq, icanet
 */
public class Action {
    
    // VARIABLES
    
    /**
     * Ligne de la grille.
     */
    private final int ligne;
    
    /**
     * Colonne de la grille.
     */
    private final int colonne;
    
    /**
     * Type action.
     */
    private final Type action;
    
    /**
     * Nombre de pions de chaque couleur.
     */
    private int[] pions;
    
    //CONSTRUCTEURS
    
    /**
     * Constructeur de la classe Action.
     * @param ligne ligne de la grille
     * @param colonne colonne de la colonne
     * @param action action possible
     */
    public Action (int ligne, int colonne, Type action) {
        this.ligne = ligne;
        this.colonne = colonne;
        this.action = action;
        pions = new int[2];
    }
    
    /**
     * Constructeur de la classe Action.
     * @param action action possible
     */
    public Action (Action action) {
        this.ligne = action.getLigne();
        this.colonne = action.getColonne();
        this.action = action.getType();
        pions = new int[2];
        pions[0] = action.getPions(NOIR);
        pions[1] = action.getPions(BLANC);
    }
    
    
    /**
     * Constructeur de la classe Action.
     * @param ligne ligne de la grille
     * @param colonne colonne de la colonne
     * @param action action possible
     */
    public Action (int ligne, int colonne, char action) {
        this.ligne = ligne;
        this.colonne = colonne;
        pions = new int[2];
        
        switch (action) {
            case 'P':
                this.action = Type.POSE;
                break;
            case 'A':
                this.action = Type.ACTIVATION;
                break;
            default:
                throw new IllegalArgumentException("L'action " + action + " est illégale, seuls P et A sont autorisés.");
        }
    }
    
    public Action (String action){
        this(Utils.carLigneVersNum(action.charAt(1)),
             Utils.carColonneVersNum(action.charAt(2)),
             action.charAt(0));
    }
    
    // FONCTIONS
    
    /**
     * Réinitialise le compte de pions.
     */
    public void reinit(){
        pions[0] = 0;
        pions[1] = 0;
    }
    
    /**
     * Ajoute un certain nombre de pions à cette action.
     * @param couleur couleur des pions
     * @param pions nombre de pions
     */
    public void ajouter(Couleur couleur, int pions){
        if(pions < 0)       throw new IllegalArgumentException("Cette méthode n'autorise pas des valeurs négatives : " + pions);
        this.pions[couleur == NOIR ? 0 : 1] += pions;
    }
    
    /**
     * Enlève un certain nombre de pions à cette action.
     * @param couleur couleur des pions
     * @param pions nombre de pions
     */
    public void enlever(Couleur couleur, int pions){
        if(pions < 0)       throw new IllegalArgumentException("Cette méthode n'autorise pas des valeurs négatives : " + pions);
        this.pions[couleur == NOIR ? 0 : 1] -= pions;
    }
    
    /**
     * Renvoie le nombre de pions d'une couleur spécifiée.
     * @param couleur couleur des pions qui nous interessent
     * @return Le nombre de pions de cette couleur
     */
    public int getPions(Couleur couleur){
        return pions[couleur == NOIR ? 0 : 1];
    }
    
    public int[] getPions(){
        return pions;
    }
    
    public void setPions(int[] pions){
        this.pions = pions;
    }
    
    @Override
    public String toString (){
        String c = "";
        switch (action) {
            case POSE:
                c = "P";
                break;
            case ACTIVATION:
                c = "A";
                break;
            default:
                throw new IllegalArgumentException("L'action " + action + " ne peut pas être effectuée.");
        }
        return c // action = Activation
            + Utils.numVersCarLigne(ligne) // convertit la ligne en lettre
            + Utils.numVersCarColonne(colonne); // convertit la colonne en lettre   
    }
    
    /**
     * Calcule le score de cette action.
     * <p>Le score est calculé par : nbre_alliés - nbre_ennemis
     * @param couleur le joueur souhaite connaître le score
     * @return Le score de cette action.
     */
    public int score(Couleur couleur){
        return getPions(couleur) - getPions(couleur.inverse());
    }
    
    /**
     * Permet de savoir si une action est meilleure qu'une autre.
     * @param a l'autre action
     * @param couleur le joueur qui souhaite connaître la réponse
     * @return <code>true</code> si cette action est meilleure que l'autre.
     */
    public boolean estMeilleureQue(Action a, Couleur couleur){
       return score(couleur) >= a.score(couleur);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + this.ligne;
        hash = 43 * hash + this.colonne;
        hash = 43 * hash + Objects.hashCode(this.action);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Action other = (Action) obj;
        if (this.ligne != other.ligne) {
            return false;
        }
        if (this.colonne != other.colonne) {
            return false;
        }
        if (this.action != other.action) {
            return false;
        }
        if (!Arrays.equals(this.pions, other.pions)) {
            return false;
        }
        return true;
    }
    
    // GETTERS / SETTERS
    
    /**
     * Récupère le numéro de la ligne.
     * @return l'attribut ligne
     */
    public int getLigne() {
        return ligne;
    }
    
    /**
     * Récupère le numéro de la colonne.
     * @return l'attribut colonne
     */
    public int getColonne() {
        return colonne;
    }
    
    /**
     * Récupère le type de l'action.
     * @return la variable action
     */
    public Type getType() {
        return action;
    }
    
    // STATIC

    /**
     * Type d'action.
     */
    public enum Type {
        /** L'action est une pose ou une pose double. */
        POSE,
        /** L'action est une activation. */
        ACTIVATION;
        
        public final char toChar(){
            return this == POSE ? 'P' : 'A';
        }
    }
}
